import React from 'react'
import { Desktop, LargeDesktop, Mobile, Tablet } from '../../Layout/Responsive'
import TabsNewsDesktop from './TabsNewsDesktop'
import TabsNewsMobTab from './TabsNewsMobTab'

export default function TabsNewsLayout() {
  return (
    <div>
      <Mobile>
        <TabsNewsMobTab/>
      </Mobile>
      <Tablet>
        <TabsNewsMobTab/>
      </Tablet>
      <Desktop>
        <TabsNewsDesktop/>
      </Desktop>
      <LargeDesktop>
        <TabsNewsDesktop/>
      </LargeDesktop>
    </div>
  )
}
