import React from 'react'
import ReactPlayer from 'react-player';
import Modal from 'react-modal';
import 'animate.css';
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal } from '../../Toolkits/modalSlice';

Modal.setAppElement('#root');
function ModalPlayer() {
    let dispatch = useDispatch() ; 
    let handleClose = () => {
        dispatch(handleCloseModal()) ; 
    }

    let {url , isOpen} = useSelector(state => state.modalSlice) ; 
    return (
            <Modal style={{overlay : {zIndex : 20} , content : {background : 'rgb(255, 255, 255 , .3)'}}} className='h-screen w-screen' isOpen={isOpen} onRequestClose={handleClose}>
                <div className='h-full w-full animate__animated animate__backInDown flex justify-center items-center z-50 cursor-pointer' onClick={handleClose}>
                    <ReactPlayer width={800} height={400} url={url} playing={isOpen} />
                </div>
                <button className='absolute top-24 right-5 z-50 text-5xl text-black' onClick={handleClose}>X</button>
            </Modal>
    )
}

export default ModalPlayer ;