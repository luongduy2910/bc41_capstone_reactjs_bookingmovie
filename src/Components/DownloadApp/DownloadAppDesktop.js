import React from 'react';
import { Carousel } from 'antd';

export default function DownloadAppDesktop() {
  return (
    <div id='downloadApp' className='downloadAppDesktop'>
        <div className="container">
            <div className="grid grid-cols-2">
                <div className="downloadApp__content text-white pt-5">
                    <p>Ứng dụng tiện lợi dành cho</p>
                    <p>người yêu điện ảnh</p>
                    <p>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p>
                    <a className='downloadButton' target='_blank' href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">APP MIỄN PHÍ - TẢI VỀ NGAY!</a>
                    <p>
                        TIX có hai phiên bản
                        <a href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">IOS</a>
                        &
                        <a href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"> Android</a>
                    </p>
                </div>
                <div className="downloadApp__carousel">
                    <Carousel effect="fade" autoplay>
                        <div>
                            <img src="./images/app_carousel1.jpg" alt="" />
                        </div>
                        <div>
                            <img src="./images/app_carousel2.jpg" alt="" />
                        </div>
                        <div>
                            <img src="./images/app_carousel3.jpg" alt="" />
                        </div>
                        <div>
                            <img src="./images/app_carousel4.jpg" alt="" />
                        </div>
                        <div>
                            <img src="./images/app_carousel5.jpg" alt="" />
                        </div>
                    </Carousel>
                </div>
            </div>
        </div>
    </div>
  )
}
