import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderUserInfo from './HeaderUserInfo';

function Headers() {
  return (
    <>
      {['lg'].map((expand) => (
        <Navbar fixed='top' key={expand} expand={expand} style={{background : 'rgb(255, 255, 255 , .9)'}} className="mb-3 h-20 shadow-md font-bold z-50">
          <Container fluid>
            <Navbar.Brand href="/"><img src="https://demo1.cybersoft.edu.vn/logo.png" className='w-48' alt="logo" /></Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  CyberFlix
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body className='items-center lg:space-x-10'>
                <Nav className="justify-content-end flex-grow-1 pe-3">
                  <Nav.Link className='mr-5 border-b-2 pb-6 border-gray-300 lg:border-b-0 nav__link' href="#lich__chieu">Lịch Chiếu</Nav.Link>
                  <Nav.Link className='mr-5 border-b-2 pb-6 border-gray-300 lg:border-b-0 nav__link' href="#tabsMovie">Cụm Rạp</Nav.Link>
                  <Nav.Link className='mr-5 border-b-2 pb-6 border-gray-300 lg:border-b-0 nav__link' href="#tabsNews">Tin Tức</Nav.Link>
                  <Nav.Link className='mr-5 border-b-2 pb-6 border-gray-300 lg:border-b-0 nav__link' href="#footer">Ứng dụng</Nav.Link>
                </Nav>
                <HeaderUserInfo/>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  );
}

export default Headers;

