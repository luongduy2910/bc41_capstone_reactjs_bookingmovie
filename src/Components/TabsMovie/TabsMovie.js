import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { movieServ } from '../../Services/movieService';
import ItemTabsMovie from './ItemTabsMovie';

export default function TabsMovie() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);
  useEffect(() => {
    movieServ.getMovieTheater()
             .then((res) => {
                setDanhSachHeThongRap(res.data.content);
             })
             .catch((err) => {
                console.log(err);
             })
  },[]);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
        return {
            key: heThongRap.maHeThongRap,
            label: (
                <div className='logoTheater'>
                    <img src={heThongRap.logo} alt="" className='w-full object-contain'/>
                </div>
            ),
            children: (<Tabs tabPosition='left' style={{height: 720}} defaultActiveKey="1" items={heThongRap.lstCumRap.slice(0,8).map((cumRap) => {
                return {
                    key: cumRap.tenCumRap,
                    label: (
                        <div className='detailTheater text-left w-full'>
                            <h4>{cumRap.tenCumRap}</h4>
                            <h6>{cumRap.diaChi}</h6>
                            <a href="/">[chi tiết]</a>
                        </div>
                    ),
                    children: (
                        <div style={{height: 720, overflowY: 'scroll'}}>
                        {cumRap.danhSachPhim.map((phim) => {
                            return <ItemTabsMovie phim={phim} key={phim.maPhim}/>;
                        })}
                    </div>
                    ),
                }      
            })}/>),
        }
    })
  }

  return (
    <div id='tabsMovie'>
        <div className="container">
            <div className='headerShadow__tabsMovie'></div>
            <Tabs tabPosition='left' defaultActiveKey="1" items={renderHeThongRap()} style={{height: 720}}/>
        </div>
    </div>
  )
}
