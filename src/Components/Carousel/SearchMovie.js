import { Button, Select } from 'antd';
import React , {useState , useEffect} from 'react'
import { movieServ } from '../../Services/movieService';
import moment from 'moment';

function SearchMovie() {
    const [listmovie, setListmovie] = useState([]) ; 
    const [cumRap, setCumRap] = useState([]) ; 
    const [lichChieu, setLichChieu] = useState([]) ;
    const [ngayChieu, setNgayChieu] = useState([]) ; 
    const handleChangeMovie = (value) => {
        let fetchLichChieuPhim = async () => {
            try {
                let response = await movieServ.getTheaterDetailedMovie(value) ; 
                console.log(response.data.content);
                let data = response.data.content ; 
                let cumRap = data.heThongRapChieu.map(item => {
                    return item.cumRapChieu
                }) ; 
                let tenCumRap = [] ;
                cumRap.forEach(item => {
                    item.forEach(tenRap => {
                        let ten =  {
                            value : tenRap.maCumRap , 
                            label : tenRap.tenCumRap , 
                        }
                        tenCumRap.push(ten) ; 
                    })
                }) ; 
                console.log(tenCumRap);
                setCumRap(tenCumRap) ;
                console.log(cumRap);
                setLichChieu(cumRap) ; 
            } catch (error) {
                console.log(error);
            }
        }
        fetchLichChieuPhim() ; 
    };
    let handleChangeTheater = (value) => {
        console.log(value);
        lichChieu.forEach(item => {
            let rapCanTim = item.filter(rap => rap.maCumRap === value) ; 
            rapCanTim.forEach(item => {
                let ngayGio = item.lichChieuPhim.map(date => {
                    return {
                        value : date.maRap , 
                        label : moment(date.ngayChieuGioChieu).format('L~hh:ss') ,
                    }
                }) ;
                console.log(ngayGio); 
                setNgayChieu(ngayGio) ; 
            })
        })
    }

    useEffect(() => {
    let fetchListMovie = async () => {
        try {
            let response = await movieServ.getListMovie() ; 
            let data = response.data.content ; 
            let list = data.map((movie)=> {
                return {
                    value : String(movie.maPhim) , 
                    label : movie.tenPhim , 
                }
            }) ; 
            setListmovie(list) ;
        } catch (error) {
            console.log(error);
        }
    }
    fetchListMovie() ; 
        
    }, [])
    
    return (
        <div style={{zIndex : '15'}} className='grid grid-cols-4 lg:w-3/5 lg:mx-auto md:w-11/12 md:mx-auto p-4 rounded bg-white shadow absolute left-1/2 -translate-x-1/2 -bottom-10'>
            <Select
            bordered = {false}
            style={{
                borderRight : '.5px solid grey'
            }}
            placeholder="Chọn phim ..."
            onChange={handleChangeMovie}
            options={listmovie}
        />
        <Select
            bordered = {false}
            style={{
                borderRight : '.5px solid grey'
            }}
            placeholder="Chọn rạp chiếu..."
            onChange={handleChangeTheater}
            options={cumRap}
        />
        <Select
            bordered = {false}
            style={{
                borderRight : '.5px solid grey'
            }}
            placeholder="Chọn khung giờ ..."
            options={ngayChieu}
        />
        <Button className='ml-10' type='primary' size='middle' danger>Mua vé</Button>
        </div>
    )
}

export default SearchMovie

