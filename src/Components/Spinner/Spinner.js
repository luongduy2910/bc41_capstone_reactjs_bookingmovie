import React from 'react'
import { useSelector } from 'react-redux'
import { HashLoader } from 'react-spinners'

// TODO: sử dụng thư viện react-spinners để tạo hiệu ứng loading cho website

function Spinner() {
    let {isLoading} = useSelector((state) => state.spinnerSlice) ; 
    if (isLoading) {
        return (
            <div style={{background : 'rgb(255, 255, 255 , 0.7)'}} className='w-screen h-screen fixed top-0 left-0 flex justify-center items-center z-50'>
                <HashLoader color="#36d7b7" />
            </div>
        )
    }else {
        return <></>
    }
}

export default Spinner