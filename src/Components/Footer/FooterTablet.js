import React from 'react'

export default function FooterTablet() {
  return (
    <div id='footer' className='footerTablet'>
      <div className='upperFooter container py-7 grid grid-cols-3'>
        <div className='text-xs text-center space-y-3'>
          <h6>Thỏa thuận sử dụng</h6>
          <h6>Chính sách bảo mật</h6>
        </div>
        <div className='flex justify-center space-x-3'>
          <a target='_blank' href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197">
            <img className='w-8 object-contain' src="./images/icon_fb.png" alt=""/>
          </a>
          <a target='_blank' href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123">
            <img className='w-8 object-contain' src="./images/icon_zalo.png" alt=""/>
          </a>
        </div>
      </div>
      <div className='lowerFooter container px-10 py-7 flex justify-between align-bottom'>
        <div><img src="./images/download.jfif" alt="" /></div>
        <div className='text-white text-center text-xs px-10 leading-5'>
          <h6 className='mb-2'>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h6>
          <p>Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam. <br />
              Giấy chứng nhận đăng ký kinh doanh số: 0101659783, <br />
              đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp. <br />
              Số Điện Thoại (Hotline): 1900 545 436</p>
        </div>
        <div><img src="./images/daThongBao-logo.png" alt="" /></div>
      </div>
    </div>
  )
}
