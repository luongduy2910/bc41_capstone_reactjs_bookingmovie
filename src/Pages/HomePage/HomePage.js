import React from 'react'
import Carousels from '../../Components/Carousel/Carousel'
import TabsMovieLayout from '../../Components/TabsMovie/TabsMovieLayout'
import MovieList from '../../Components/MovieList/MovieList'
import ModalPlayer from '../../Components/Modal/Modal'
import DownloadAppLayout from '../../Components/DownloadApp/DownloadAppLayout'
import TabsNewsLayout from '../../Components/TabsNews/TabsNewsLayout'

function HomePage() {
  return (
    <div>
      <ModalPlayer/>
      <Carousels/>
      <MovieList/>
      <TabsMovieLayout/>
      <TabsNewsLayout/>
      <DownloadAppLayout/>
    </div>
  )
}

export default HomePage