import React from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { userServ } from '../../Services/userService';
import { useDispatch } from 'react-redux';
import { setLoginUser } from '../../Toolkits/userSlice';
import { localUserServ } from '../../Services/localService';

function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let fillForm = () => {
    let info = localUserServ.get();
    if (info != null) {
      console.log(info);
      return info;
    }
    else {
      return {taiKhoan: "", matKhau: ""}
    }
  };
  const onFinish = (values) => {
    userServ.loginUser(values)
            .then((res) => {
              message.success("Đăng nhập thành công");
              dispatch(setLoginUser(res.data.content));
              localUserServ.set(res.data.content);
              // * sau 1.5s sẽ chuyển tới trang đăng nhập
              setTimeout(() => {
                navigate('/') ;
              } , 1500) ;
            })
            .catch((err) => {
              message.error("Đăng nhập thất bại");
              console.log(err);
            })
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  
  return (
    <div className='loginPage text-2xl bg-white rounded-xl p-5 sm:w-2/3 md:w-2/5 lg:w-1/3'>
      <div className='loginTitle text-center w-full space-y-5 mb-10'>
        <i style={{lineHeight : "45px" , borderRadius : '50%'}} className="fas fa-user fa-lg fa-fw bg-orange-500 w-12 h-12 text-white text-2xl"></i>
        <h2 className='text-3xl font-bold text-red-700 '>Đăng nhập</h2>
      </div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
          taiKhoan: fillForm().taiKhoan,
          matKhau: fillForm().matKhau,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout='vertical'
        className='space-y-8'
      >
        <Form.Item
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Đây là trường bắt buộc!',
            },
          ]}
          
        >
          <Input className='py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600' placeholder="Tài Khoản *"/>
        </Form.Item>

        <Form.Item
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Đây là trường bắt buộc!',
            },
          ]}
        >
          <Input.Password className='py-3 text-gray-800 hover:border-orange-600 focus:border-orange-600' placeholder="Mật Khẩu *"/>
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Checkbox>Nhớ tài khoản</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Button type="primary" htmlType="submit" className='loginButton bg-orange-600 text-white mb-3 py-6 block w-full'>
            Đăng nhập
          </Button>
          <NavLink to={'/register'} className="text-right block hover:underline underline-offset-2 decoration-black hover:decoration-black">
            <span className='text-black font-medium'>Bạn chưa có tài khoản? Đăng ký</span>
          </NavLink>
        </Form.Item>
      </Form>
    </div>
  )
}

export default LoginPage