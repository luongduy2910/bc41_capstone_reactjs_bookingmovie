import { Button, Form, Input, message } from 'antd';
import { userServ } from '../../Services/userService';
import { NavLink, useNavigate } from 'react-router-dom';
import { validateEmail, validateEqualMk, validateHt, validateMk, validatePhoneVietNam, validateTk } from './ValidationForm';
import { useDispatch } from 'react-redux';
import { closeFormSuccess, handleOk, registerSuccess } from '../../Toolkits/formSuccessSlice';
import React from 'react'
import SuccessForm from '../../Components/SuccessForm/SuccessForm';
import { localUserServ } from '../../Services/localService';

function RegisterPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate() ;   
  const onFinish = async (values) => {
    let cloneValues = {...values , maNhom : 'GP09'} ; 
    let {taiKhoan , matKhau , matKhauNhapLai , hoTen , email , soDt} = cloneValues;
    let fetchRegisterUser = async () => {
      let isValid = validateTk(taiKhoan) && validateMk(matKhau) && validateEqualMk(matKhau , matKhauNhapLai) && validateHt(hoTen) && validateEmail(email) && validatePhoneVietNam(soDt) ; 
      if (isValid) {
        try {
          let response = await userServ.registerUser(values) ; 
          console.log(response);
          dispatch(registerSuccess()) ; 
          localUserServ.set(response.data.content);
          // * sau 2s sẽ tắt thông báo 
          setTimeout(() => {
            dispatch((closeFormSuccess())) ; 
          } , 2000) ; 
          // * sau 3s sẽ chuyển tới trang đăng nhập 
          setTimeout(() => {
            navigate('/login') ;
          } , 3000) ; 
        } catch (error) {
          console.log(error);
          message.error(error.response.data.content) ; 
        }
    }
    }
    fetchRegisterUser() ; 
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
  <>
    <SuccessForm/>
    <Form
      name="basic"
      labelCol={{
        span: 24,
      }}
      wrapperCol={{
        span: 24,
      }}
      className='sm:w-2/3 md:w-3/5 lg:w-1/3'
      style={{
        background : 'white' ,
        borderRadius : '20px',
        padding : '15px 50px' ,
        
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout='vertical'
    >
      <div className='text-center w-full space-y-5 mb-10'>
        <i style={{lineHeight : "45px" , borderRadius : '50%'}} className="fas fa-lock fa-lg fa-fw bg-orange-500 w-12 h-12"></i>
        <h2 className='text-3xl font-bold text-red-700 '>Đăng ký</h2>
      </div>
      <Form.Item
      label = 'Tài khoản 6 ký tự trở lên gồm chữ và số'
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: 'Nhập tài khoản vào đây',
            
          },
        ]}
      >
        <Input className='py-3 placeholder:font-bold text-gray-800 hover:border-orange-600' placeholder = "nhập tài khoản ..." />
      </Form.Item>
      <Form.Item
        name="matKhau"
        label = "Mật khẩu gồm 4 ký tự trở lên"
        rules={[
          {
            required: true,
            message: 'Nhập mật khẩu vào đây',
          },
        ]}
      >
        <Input.Password className="input__password py-3 placeholder:font-bold text-gray-800 hover:border-orange-600" placeholder = "nhập mật khẩu ..." />
      </Form.Item>
      <Form.Item
        name="matKhauNhapLai"
        rules={[
          {
            required: true,
            message: 'Nhập lại mật khẩu vào đây',
          },
        ]}
      >
        <Input.Password className='input__password py-3 placeholder:font-bold text-gray-800 hover:border-orange-600' placeholder = "nhập lại mật khẩu ..." />
      </Form.Item>
      <Form.Item
      label = 'Họ Tên không chứa ký tự đặc biệt và số'
        name="hoTen"
        rules={[
          {
            required: true,
            message: 'Nhập Họ Tên vào đây',
          },
        ]}
      >
        <Input className='py-3 placeholder:font-bold text-gray-800 hover:border-orange-600' placeholder = "nhập họ tên ..." />
      </Form.Item>
      <Form.Item
      label = 'nhập email hợp lệ theo định dạng abc@gcd.xy'
        name="email"
        rules={[
          {
            required: true,
            message: 'Nhập email vào đây',
          },
        ]}
      >
        <Input className='py-3 placeholder:font-bold text-gray-800 hover:border-orange-600' placeholder = "nhập email ..." />
      </Form.Item>
      <Form.Item
      label = "số điện thoại gồm 10 số"
        name="soDt"
        rules={[
          {
            required: true,
            message: 'Nhập số điện thoại vào đây',
          },
        ]}
      >
        <Input className='py-3 placeholder:font-bold text-gray-800 hover:border-orange-600' placeholder = "nhập số điện thoại ..." />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 24,
        }}
      >
        <Button style={{lineHeight : '5px'}} className='bg-blue-500 text-white mb-5 py-6 block w-full' type="primary" htmlType="submit">
          Đăng ký 
        </Button>
      <NavLink style={{textDecoration : 'underline'}} to={'/login'} className="text-right block">
        <span className='text-purple-900'>Bạn đã có tài khoản?Đăng nhập</span>
      </NavLink>
      </Form.Item>
    </Form>
  </>
  )
}

export default RegisterPage