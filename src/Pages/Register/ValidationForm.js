// TODO: file dùng để validate các ô input trong form đăng ký 
// * ô tài khoản : gồm 6 ký tự trở lên , bao gồm chữ và số 
import { message } from "antd";
import validator from "validator";

export const validateTk = (taiKhoan) => {
    let flag = false;
    if (validator.matches(taiKhoan, /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/)) {
        flag = true;
    } else {
        message.error('Tài khoản ít nhất 6 ký tự bao gồm cả chữ và số!')
    }
    return flag;
}

// * ô mật khẩu : gồm 4 ký tự trở lên
export const validateMk = (matKhau) => {
    let flag = false;
    const { isLength } = require('validator');
    const isValidPassword = isLength(matKhau.trim(), { min: 4 });
    if (isValidPassword) {
        flag = true;
    }else {
        message.error('Mật khẩu ít nhất 4 ký tự trở lên!') ; 
    }
    return flag;
}

// * kiểm tra mật khẩu và mật khẩu nhập lại có trùng nhau hay không 
export const validateEqualMk = (matKhau , matKhauNhapLai) => {
    let flag = false;
    const { equals } = require('validator');
    const arePasswordsEqual = equals(matKhau, matKhauNhapLai);
    if (arePasswordsEqual) {
        flag = true;
    }else {
        message.error('Mật khẩu không trùng khớp với nhau!') ; 
    }
    return flag;
}

// * kiểm tra họ Tên : chuỗi là chữ không chứa số và ký tự đặc biệt 

export const validateHt = (hoTen) => {
    let flag = false;
    if (validator.isLength(hoTen, { min: 3, max: 50 }) && validator.matches(hoTen, /^[\p{L} ]+$/u)) {
        flag = true;
    } else {
        message.error('Họ tên không hợp lệ');
    }
    return flag
}

// * kiểm tra email : hợp lệ với email cơ bản nguyenvana@gmail.com 

export const validateEmail = (email) => {
    let flag = false ; 
    const validator = require('validator');
    // Kiểm tra email
    if (validator.isEmail(email)) {
    flag = true;
    } else {
    message.error('Email không hợp lệ') ; 
    }
    return flag ; 
}

// * kiểm tra số diện thoại : số điện thoại theo chuẩn Việt Nam quy định gồm 10 số 

export const validatePhoneVietNam = (soDt) => {
    let flag = false;
    const validator = require('validator');
// Kiểm tra số điện thoại ở Việt Nam
    if (validator.isMobilePhone(soDt, 'vi-VN')) {
        flag = true;
    } else {
        message.error('Số điện thoại không hợp lệ!') ; 
    }
    return flag ; 
} 