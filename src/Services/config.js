import axios from "axios"

const baseURL = 'https://movienew.cybersoft.edu.vn' ;
const TokenCyberSoft = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q'
const configHeader = () => {
    return {
        // * Nơi chứa token học viên 
        TokenCyberSoft : TokenCyberSoft , 
        // * nơi chứa token access của acount (tạo ra sau khi đăng nhập tài khoản thành công)
    }
}

// TODO: sử dụng axios trung gian để gọi api 

export const https = axios.create({
    baseURL : baseURL , 
    headers : configHeader() , 
})