import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isModalVisible : false,
}

const formSuccessSlice = createSlice({
    name: "formSuccessSlice",
    initialState,
    reducers: {
        registerSuccess : (state , action) => {
            state.isModalVisible = true;
        } , 
        handleOk : (state , action) => {
            state.isModalVisible = false;
        } , 
        closeFormSuccess : (state , action) => {
            state.isModalVisible = false;
        }
    }
});

export const {registerSuccess , handleOk , closeFormSuccess} = formSuccessSlice.actions ; 

export default formSuccessSlice.reducer