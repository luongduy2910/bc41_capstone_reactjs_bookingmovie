/** @type {import('tailwindcss').Config} */
module.exports = {
  // * cấu hình thêm jit để tối ưu hiệu suất cho tailwindcss
  mode : 'jit' , 
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

